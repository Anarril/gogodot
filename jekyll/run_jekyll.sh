#!/bin/sh
sudo docker run \
	 --rm --volume="$PWD:/srv/jekyll" -it jekyll/jekyll:$JEKYLL_VERSION\
	jekyll serve --incremental --watch
