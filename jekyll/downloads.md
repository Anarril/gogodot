---
layout: page
title: Downloads
permalink: /downloads/
---

Download your game here:
* [Linux][linux-download]
* [Mac OSX][osx-download]
* [Web][web-download]
* [Windows][windows-download]

[linux-download]: {{ site.baseurl }}/build/example_linux.tgz
[osx-download]: {{ site.baseurl }}/build/example_osx.zip
[web-download]: {{ site.baseurl }}/build/example_web.html
[windows-download]: {{ site.baseurl }}/build/example_windows.zip
