---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
See README.md in the project root for more details/usage info/etc. and the 
[downloads]({{ site.baseurl }}/downloads) page for downloads of the example "game".
