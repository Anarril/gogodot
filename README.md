# gogodot
[![pipeline status](https://gitlab.com/Anarril/gogodot/badges/master/pipeline.svg)](https://gitlab.com/Anarril/gogodot/commits/master)
[![image](https://img.shields.io/badge/license-MIT-blue.svg?longCache=true)](https://gitlab.com/Anarril/gogodot/raw/master/LICENSE.md)
[![image](https://img.shields.io/badge/hosting-gitlab-orange.svg?longCache=true)](https://gitlab.com/)

The goal of gogodot is to make your Godot game available to the world for most of its supported platforms (along with 
a simple website), while taking as little of your time possible. This means you can focus on making the actual game. 
This is especially handy during game jams, where both time pressure and publicizing your game are important factors.
It does this by providing a directory structure and config files that tell Gitlab how to do all this.

You can see an example of the website output over here: https://anarril.gitlab.io/gogodot/

Although gogodot out of the box is focused on quick projects with Gitlab + Godot + Jekyll, everything involved is open 
source and open to change. Adapt it if you want and please let me know if you do!

## What does gogodot do:
* automatic builds of your game with every change you push to Gitlab for most platforms
* automatic updating of a corresponding website, who's content you control via simple but powerful of Jekyll + Gitlab pages
* no paid services/subscriptions
* automatic running of Godot unit tests (via Gut) 
* while a certain level of technical affinity is assumed, you don't have to know CI/CD, Gitlab internals, Docker or most 
of how Jekyll works

## What DOESN'T gogodot do:
* support exporting to iOS (ever, probably)
* support exporting to Android (yet)
* allow download of older versions of your game
* promise to remain updated after my initial work on it
* promise to be tested in all circumstances/environments/platforms

## What do you have to know/have to be able to use gogodot:
* [Godot](https://godotengine.org/)
* [git](https://git-scm.com)
* some experience with [Jekyll](https://jekyllrb.com/docs/) is handy, primarily with 
[markdown](https://daringfireball.net/projects/markdown/) ([cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet))
* a [Gitlab](https://gitlab.com) account

## How do you use gogodot 
* Create a new project in Gitlab.
* Clone/download this repository.
* Make sure the base directory is a git repository that pushes to your new project on Gitlab.
* Open the `/godot` directory with the Godot editor.
* Edit the Jekyll files in `/jekyll` with your editor of preference.
* You can safely remove this README.md, but not `.git`, `.gitignore` or `gitlab-ci.yaml`. I don't think there are many 
files inside the `/godot` and `/jekyll` directories that you'd want to remove, as both are almost as empty as when 
Godot/Jekyll generates a new project.
* If you push changes in this project to Gitlab, it will run any Gut tests in `/godot/test` and build/export your game. 
If you pushed in the master branch, the Gitlab pages website will be updated and the updated exports of your game will 
be made available.

## FAQ:
__Q: Why this name?__  
A: it's meant to resemble the "Go! Go! Go!" call for action, but with Godot mixed in. Because it is about speedy/hurried
Godot development.

__Q: Will you add...__   
A: Probably not.    

__Q: Why not?__  
A: Many reasons, but mostly I just don't want to spend time on it. I made this for fun, and what I think is entertaining
changes often. You are free to make your own improvements though!
